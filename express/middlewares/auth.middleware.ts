import {Request, Response, NextFunction} from "express";
import {ExpressUtils} from "../express.utils";
import {AuthService} from "../../axios";
import {ISession} from "@sottiz/eth-lib";

declare module 'express' {
    interface Request {
        session?: ISession;
    }
}

export function authMiddleware() {
    return async function(req: Request, res: Response, next: NextFunction) {
        const token = ExpressUtils.extractToken(req);
        if(!token) {
            res.status(401).end();
            return;
        }
        const session = await AuthService.getShared().getSession(token);
        if(session === null) {
            res.status(403).end();
            return;
        }
        req.session = session;
        next();
    }
}