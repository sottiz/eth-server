import { ISession } from "@sottiz/eth-lib";
import { Axios, AxiosResponse, RawAxiosRequestHeaders } from "axios";
const axios = require('axios');

export class AuthService {

    readonly instance: Axios;
    private static shared: AuthService;
    
    constructor() {
        this.instance = axios.create({
            baseURL: process.env.ETH_AUTH_URI
        });
    }

    public static getShared(): AuthService {
        if (this.shared === undefined) {
            this.shared = new AuthService();
        }
        return this.shared;
    }
    
    public async getSession(token: string): Promise<ISession | null> {
        try {
            const res: AxiosResponse<ISession> = await this.instance.get('/account/me', {
                headers: {
                    Authorization: `Bearer ${token}`
                } as RawAxiosRequestHeaders
            });
            return res.data;
        } catch (err) {
            console.error(err);
            return null;
        }
    }
}
